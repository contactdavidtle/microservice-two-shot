from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from .models import Shoe, BinVO


class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "closet_name",
        "bin_number",
        "bin_size",
        "import_href",
        "id",
    ]


class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "name",
        "manufacturer",
        "model_name",
        "color",
        "picture_url",
        "id",
        "bin",
        ]

    encoders = {"bin": BinVOEncoder()}


class ShoesListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "name",
        "manufacturer",
        "model_name",
        "color",
        "picture_url",
        "id",
        "bin",
        ]

    encoders = {"bin": BinVOEncoder()}


@require_http_methods(["GET", "POST"])
def api_list_shoes(request):
    """
    {
        "shoes" : {
            "name": Name of shoe,
            "id":
        }

    }

    """
    if request.method == "GET":
        shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoesListEncoder
        )

    else:  # POST
        content = json.loads(request.body)

        bin_href = content["bin"]
        bin = BinVO.objects.get(import_href=bin_href)
        content["bin"] = bin

        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "DELETE"])
def api_show_shoe(request, id):
    """
    {
        "shoes": [
            "name": Name of shoe,
            "manufacturer": Manufacturer of shoe,
            "model_name": Model name of shoe,
            "color": shoe color,
            "picture_url": url to picture,
            "bin": The bin the shoe is placed in
            ""
        ]
    }
    """
    if request.method == "GET":
        shoe = Shoe.objects.get(id=id)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )

    else:  # Delete
        count, _ = Shoe.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
