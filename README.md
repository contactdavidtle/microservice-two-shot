# Wardrobify

Team:

* David Le - Shoes
* Ana María Pedroza - Hats

## Design

## Shoes microservice

The shoes microservice contains shoes models which contains as properites "Shoe name, shoe manufacturer, shoe model, picture of shoe, bin it is placed in and color of the shoe. It polls from the Wardrobe microservice to get bin and places it as a value object. It allows for getting a list of shoes, getting a specific shoe, deleting a shoe, and creating a shoe.

## RESTful API endpoints: Shoes
DELETE SHOES - DELETE ---------> http://localhost:8080/api/shoes/int:pk/
DETAIL SHOES - GET  ------------> http://localhost:8080/api/shoes/int:pk/
LIST SHOES - GET  ------------> http://localhost:8080/api/shoes/
CREATE SHOES - POST ------------> http://localhost:8080/api/shoes/

# RESTful API endpoints: Monolith bins
DELETE BINS - DELETE ---------> http://localhost:8100/api/bins/int:pk/
DETAIL BINS - GET  ------------> http://localhost:8100/api/bins/int:pk/
LIST BINS - GET  ------------> http://localhost:8100/api/bins/
CREATE BINS - POST ------------> http://localhost:8100/api/bins/

## Hats microservice

The microservice in question includes a model known as "Hat," which enables users to create instances of the Hat Model with various attributes such as fabric, style name, color, picture, and wardrobe location. The location is determined via a "Value Object" obtained from the Wardrobe API, which provides information such as closet name, section_number, shelf_number, and import href. These Value Objects provide access to the data without altering the original. It also allows new instances and objects to be created based on that data.

## RESTful API endpoints: Hats
DELETE BINS - DELETE ---------> http://localhost:8090/api/hats/int:pk/
DETAIL HAT - GET  ------------> http://localhost:8090/api/hats/int:pk/
LIST HATS - GET  ------------> http://localhost:8090/api/hats/
CREATE HAT - POST ------------> http://localhost:8090/api/hats/
UPDATE HAT - PUT ------------> http://localhost:8090/api/hats/int:pk/

# RESTful API endpoints: Monolith Location
DELETE LOCAIONs - DELETE ---------> http://localhost:8100/api/locations/int:pk/
DETAIL LOCAIONs - GET  ------------> http://localhost:8100/api/locations/int:pk/
LIST LOCATIONS - GET  ------------> http://localhost:8100/api/locations/
CREATE LOCATIONS - POST ------------> http://localhost:8100/api/locations/
UPDATE LOCAIONS - PUT -------------> http://localhost:8100/api/locations/int:pk/
