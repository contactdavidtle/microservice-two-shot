import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);


// async function loadItems() {
//   const requests = []
//   requests.push(fetch('http://localhost:8080/api/shoes/'))
//   requests.push(fetch('http://localhost:8090/api/hats/'))
//   const responses = await Promise.all(requests)

//   const shoesResponse = responses[0]
//   const hatsResponse = responses[1]

//   if (shoesResponse.ok && hatsResponse.ok) {
//     const shoesData = await shoesResponse.json()
//     const hatsData = await hatsResponse.json()
//     root.render(
//       <React.StrictMode>
//         <App shoes={shoesData.shoes} hats={hatsData.hats} />
//       </React.StrictMode>
//     );
//   } else {
//     console.error(responses);
//   }
// }
// loadItems();
