import { useEffect, useState } from "react";


function ShoeForm(props) {
  const [bins, setBins] = useState([])

  const [shoeName, setShoeName] = useState('')
  const [manufacturerName, setManufacturerName] = useState('')
  const [modelName, setModelName] = useState('')
  const [color, setColor] = useState('')
  const [pictureURL, setPictureURL] = useState('')
  const [bin, setBin] = useState('')

  const handleShoeNameChange = event => {
    const value = event.target.value
    setShoeName(value)
  }

  const handleManufacturerNameChange = event => {
    const value = event.target.value
    setManufacturerName(value)
  }

  const handleModelNameChange = event => {
    const value = event.target.value
    setModelName(value)
  }

  const handleColorChange = event => {
    const value = event.target.value
    setColor(value)
  }

  const handlePictureURLChange = event => {
    const value = event.target.value
    setPictureURL(value)
  }

  const handleBinChange = event => {
    const value = event.target.value
    setBin(value)
  }

  const handleSubmit = async (event) => {
    event.preventDefault();
    // create an empty JSON object
    const data = {};
    data.name = shoeName
    data.manufacturer = manufacturerName
    data.model_name = modelName
    data.color = color
    data.picture_url = pictureURL
    data.bin = bin


    const shoesURL = 'http://localhost:8080/api/shoes/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(shoesURL, fetchConfig);
    if (response.ok) {
      const newShoe = await response.json();
      console.log(newShoe);
      setShoeName('')
      setManufacturerName('')
      setModelName('')
      setColor('')
      setPictureURL('')
      setBin('')
      props.getShoes()

    }
  }

  const fetchData = async () => {
    const url = 'http://localhost:8100/api/bins/';
    const response = await fetch(url);
    if (response.ok) {
        const data = await response.json();
        setBins(data.bins)

    }
  }
    useEffect(() => {
        fetchData();
      }, []);

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new shoe</h1>
          <form onSubmit={handleSubmit} id="create-presentation-form">
            <div className="form-floating mb-3">
              <input onChange={handleShoeNameChange} placeholder="Shoe Name" required type="text" name="shoe_name" id="shoe_name" className="form-control" value={shoeName} />
              <label htmlFor="shoe_name">Shoe name</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleManufacturerNameChange} placeholder="Manufacturer Name" required type="text" name="manufacturer_name" id="manufacturer_name" className="form-control" value={manufacturerName} />
              <label htmlFor="manufacturer_name">Manufacturer Name</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleModelNameChange} placeholder="Model Name" type="text" name="model_name" id="model_name" className="form-control" value={modelName} />
              <label htmlFor="model_name">Model Name</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleColorChange} placeholder="Color" required type="text" name="color" id="color" className="form-control" value={color} />
              <label htmlFor="color">Color</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handlePictureURLChange} placeholder="Picture URL" required type="url" name="picture_url" id="picture_url" className="form-control" value={pictureURL} />
              <label htmlFor="picture_url">Picture URL</label>
            </div>
            <div className="mb-3">
              <select onChange={handleBinChange} required name="bin" id="bin" className="form-select" value={bin}>
                <option value="">Choose a bin</option>
                {bins.map(bin => {
                  return (
                    <option key={bin.id} value={bin.href}>
                      {bin.bin_number}
                    </option>
                  );
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  )
}

export default ShoeForm

//     name = models.CharField(max_length=50)
//     manufacturer = models.CharField(max_length=50)
//     model_name = models.CharField(max_length=50)
//     color = models.CharField(max_length=50)
//     picture_url = models.URLField(null=True)
//     bin = models.ForeignKey(
