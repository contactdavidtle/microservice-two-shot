import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import ShoesList from './ListShoes'
import ShoeForm from './ShoeForm';
import HatsForm from './HatsForm';
import HatList from './HatList'
import Nav from './Nav';
import { useEffect, useState } from 'react';

function App(props) {
  const [hats, setHats] = useState([])
  const [shoes, setShoes] = useState([])

  const getHats = async () => {
    const hatURL = 'http://localhost:8090/api/hats'
    const hatResponse = await fetch(hatURL)

    if (hatResponse.ok) {
      const hatsData = await hatResponse.json()
      const hats = hatsData.hats
      setHats(hats)
    }
  }

  const getShoes = async () => {
    const shoesURL = 'http://localhost:8080/api/shoes'
    const shoesResponse = await fetch(shoesURL)

    if (shoesResponse.ok) {
      const shoesData = await shoesResponse.json()
      const shoes = shoesData.shoes
      setShoes(shoes)
    }
  }

  useEffect(() => {getHats();getShoes()}, [])

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route index element={<MainPage />} />
          <Route path="shoes">
            <Route index element={<ShoesList shoes={shoes} getShoes={getShoes} />} />
            <Route path="new" element={<ShoeForm getShoes={getShoes} />} />
          </Route>
          <Route path="hats">
            <Route index element={<HatList hats={hats} getHats={getHats} />} />
            <Route path="new" element={<HatsForm getHats={getHats} />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
