import React, {useEffect, useState} from "react";

function HatsForm (props){

    const [locations, setLocations] =  useState([]);
    const [fabric, setFabric] = useState('');
    const [styleName, setStyleName] = useState('');
    const [color, setColor] = useState('');
    const [urlPicture, setPicture] = useState('');
    const [location, setlocation] = useState ('');

    const handleFabricChange = (event) => {
        const value = event.target.value;
        setFabric(value)
    }
    const handlestyleNameChange = (event) => {
        const value = event.target.value;
        setStyleName(value)
    }
    const handlesColorChange = (event) => {
        const value = event.target.value;
        setColor(value)
    }
    const handleUrlPictureChange = (event) => {
        const value = event.target.value;
        setPicture(value)
    }
    const handleLocationChange = (event) => {
        const value = event.target.value;
        setlocation(value)
    }

    const handleSubmit = async (event) =>{
        event.preventDefault();

        const data = {};
        data.fabric = fabric;
        data.style_name = styleName;
        data.color = color;
        data.url_picture = urlPicture;
        data.location = location
        console.log(data)
        const hatUrl = 'http://localhost:8090/api/hats/';
        console.log(hatUrl)
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
            'Content-Type': 'application/json',
            },
        }  
        
        const response = await fetch(hatUrl, fetchConfig);
        if(response.ok){
            const newHat = await response.json();
            console.log(newHat)
            setFabric('');
            setStyleName('');
            setColor('');
            setPicture('');
            setlocation ('');
            props.getHats()
             
        }

    }
    const fetchData = async () => {
        const url = "http://localhost:8100/api/locations/"
        console.log(url)
        const response = await fetch (url)
        if (response.ok){
            const data = await response.json();
            setLocations(data.locations)
            console.log(data.locations.import_href)
        }
    } 

    useEffect (() => {
        fetchData();
    }, [])   
return (
    <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new hat</h1>
            <form onSubmit ={handleSubmit}id="create-hat-form">
              <div className="form-floating mb-3">
                <input onChange={handlestyleNameChange}placeholder="style_name" required type="text" name="style_name" id="style_name" className="form-control" value={styleName}/>
                <label htmlFor="style_name">Style Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleFabricChange}placeholder="fabric" required type="text" name="fabric" id="fabric" className="form-control" value={fabric}/>
                <label htmlFor="fabric">Fabric</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handlesColorChange}placeholder="color" required type="text" name="color" id="color" className="form-control" value={color}/>
                <label htmlFor="color">Color</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleUrlPictureChange}placeholder="url_picture" required type="text" name="url_picture" id="url_picture" className="form-control" value={urlPicture}/>
                <label htmlFor="url_picture">Picture URL</label>
              </div>
              <div className="mb-3">
                <select onChange={handleLocationChange}required name="location" id="location" className="form-select" value={location}>
                  <option value="">Choose a Location</option>
                  {locations.map(location =>{
                    return (
                        <option key={location.href} value={location.href}>
                            {location.closet_name}
                        </option>
                    )
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
      
}
export default HatsForm