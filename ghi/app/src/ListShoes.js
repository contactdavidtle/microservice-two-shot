function ShoesList(props) {

    async function handleDelete (id) {
        const shoeURL = `http://localhost:8080/api/shoes/${id}`;
        const fetchConfig = {
          method: "delete",
        };
        const response = await fetch(shoeURL, fetchConfig);
        if (response.ok) {
          const deleted = await response.json();
          console.log(deleted);
          props.getShoes()

        }
      }

    return (
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Photo</th>
            <th>Name</th>
            <th>Manufacturer</th>
            <th>Model Name</th>
            <th>Color</th>
          </tr>
        </thead>
        <tbody>
          {props.shoes.map(shoe => {
            return (
              <tr key={shoe.id}>
                <td><img src={shoe.picture_url} alt="Shoe" /></td>
                <td>{ shoe.name }</td>
                <td>{ shoe.manufacturer }</td>
                <td>{ shoe.model_name }</td>
                <td>{ shoe.color }</td>
                <td><button onClick={ () => handleDelete(shoe.id)} type="button" className="btn btn-outline-danger">Delete</button></td>
              </tr>
            );
          })}
        </tbody>
      </table>
    );
  }

  export default ShoesList;
