from django.shortcuts import render
from .models import Hat, LocationVO
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
from common.json import ModelEncoder
import json


class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "closet_name",
        "section_number",
        "shelf_number",
        "import_href",
    ]

class HatListEncoder(ModelEncoder):
    model = Hat 
    properties = [
        "id",
        "fabric",
        "style_name",
        "color",
        "url_picture",
        "location"
        ]
    encoders = {
        "location": LocationVOEncoder()
    }
    

class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        "id",
        "style_name",
        "fabric",
        "color",
        "url_picture",
        "location",
    ]
    encoders = {
        "location": LocationVOEncoder()
    }



@require_http_methods(["GET", "POST"])
def api_list_hats(request, location_vo_id=None):

    if request.method == "GET":
        if location_vo_id is not None:
            hats = Hat.objects.filter(hats=location_vo_id)
        else:
            hats = Hat.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatListEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            location_href = content["location"]
            location = LocationVO.objects.get(import_href=location_href)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message":"Invalid location id"},status=400,
            )
        hats = Hat.objects.create(**content)
        return(
            JsonResponse(
                hats, encoder=HatDetailEncoder,safe=False
            )
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_detail_hats(request, id):
    
    if request.method == "GET":
        try:
            hats = Hat.objects.get(id=id)
            return JsonResponse(
                hats,
                encoder=HatDetailEncoder,
                safe=False,
        )
        except Hat.DoesNotExist:
            return JsonResponse(
                {"message":"Invalid hat id"},status=400,
            )
    elif request.method == "DELETE":
        count, _ = Hat.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            content = json.loads(request.body)
            Hat.objects.filter(id=id).update(**content)
            hat = Hat.objects.get(id=id)
            return JsonResponse(
                hat,
                encoder=HatDetailEncoder,
                safe=False,
            )
        except Hat.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response


    
    


    


    
